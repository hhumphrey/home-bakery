define([
	'jquery'
],

function ( $ ) {

	var opt = {
		// declare jquery selectors
		$parent : $("#js-selector")
	}

	var vars = {
		// declare vars
		myVar : 0
	}

	function setup(){
		// private function
	}

	function init(){
		// public function
		console.log("example module initialised");
	}

	return {
		init: init
	}
});