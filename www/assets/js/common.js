//The build will inline common dependencies into this file.
requirejs.config({
	baseUrl: './assets/js',
	paths: {
		'jquery':                   'lib/require-jquery',
		'jqueryMobileTouch':        'lib/jquery.mobile.touch',
		'jquerySmartResize':        'plugins/jquery.smartresize'
	},
	shim: {
		'jqueryMobileTouch':         [ 'jquery' ],
		'jquerySmartResize':         [ 'jquery' ]
	}
});
