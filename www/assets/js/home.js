define([
    'jquery',
    'lib/owl.carousel'
],

function ( $ ) {

    console.log("home bootstrapper loaded");

    $(document).ready(function() {
 
      $("#carousel").owlCarousel({
        items : 3,
        lazyLoad : true,
        navigation : true,
        mouseDrag: false,
        itemsDesktop: [1199,3],
        itemsTablet: [768,3]
      });
     
    });

});