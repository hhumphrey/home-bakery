//Filename: Carousel.js

define([
	'jquery',
	'lodash',
	'text!templates/scroller/item.html',
	'bootstrap/transitions'
], function( $, _, templateItem, logo_light, logo_dark ){
	
	function Carousel( $parent ){
		this.opts = {
			$parent: $parent,
			$scroll: $parent.find( '.carousel-inner' ),
			$leftnav: $parent.find( '.carousel-control.left'),
			$rightnav: $parent.find( '.carousel-control.right'),
	        $window: $( window )
		},
		this.vars = {
			intVisibleItems: 3,
	        intCurrentStart: 0,
	        intCurrentEnd: 0,
	        intImageWidth: 749,
	        intCurrentImageWidth: 0,
	        arrItems: [],
			activeColourState: 'light',
			animating: false
		}
		
		this.resetActive = function(){
			this.opts.$scroll.find( '.active' ).removeClass( 'active' );
			this.opts.$scroll.find( '.item:eq(1)').addClass( 'active' );
		}
		
		this.shiftScroller = function( intToShift ){
			var intCurrentLeft = parseInt( this.opts.$scroll.css( 'left' ) );
			this.opts.$scroll.css( 'left', intCurrentLeft + intToShift );
		}
		
		this.onPrevMoveComplete = function(){
			this.removeFromEnd();
			this.resetActive();
			this.checkBranding();
			
			this.vars.animating = false;
		}
		
		this.onPrevClick = function( ){
			var _this = this;
			
			if( $.support.transition ){
				this.opts.$scroll.one( $.support.transition.end, function () {
					_this.opts.$scroll.removeClass( 'with-transition' );
					_this.onPrevMoveComplete();
		        });
			}
				
				
			
			var intImageWidth = $( _.first( this.opts.$scroll.find( '.item' ) ) ).outerWidth( true );
			
			this.vars.intCurrentEnd = ( this.vars.intCurrentEnd - 1 ) < 0 ? ( this.vars.arrItems.length - 1 ) : ( this.vars.intCurrentEnd-1);
	        this.vars.intCurrentStart = ( this.vars.intCurrentStart - 1 ) < 0 ? ( this.vars.arrItems.length - 1 ) : ( this.vars.intCurrentStart-1);
			
			$.preload( [ this.vars.arrItems[ this.vars.intCurrentStart ].image ], {
				loaded_all: function( loaded, total ) {
					// add item to start
			        _this.addItemToStart( _this.vars.intCurrentStart );
			        // move
			        _this.shiftScroller( -intImageWidth );
			        // calculate movement
			        var intToMove = $( _.first( _this.opts.$scroll.find( '.item' ) ) ).outerWidth( true );
			        // move
					_this.opts.$scroll.addClass( 'with-transition' );
			        _this.shiftScroller( intToMove );

					if( !$.support.transition )
						_this.onPrevMoveComplete();
				}
			});
			
			
	        
		}
		
		this.onNextMoveComplete = function( _this ){
			var intImageWidth = $( _.first( this.opts.$scroll.find( '.item' ) ) ).outerWidth( true );

	        this.removeFromStart();
	        this.shiftScroller( intImageWidth );
			this.resetActive();
			this.checkBranding();
			this.vars.animating = false;
		}
		
		this.onNextClick = function( ){
			var _this = this;
			
			
			if( $.support.transition ){
				this.opts.$scroll.one( $.support.transition.end, function () {
					_this.opts.$scroll.removeClass( 'with-transition' );
					_this.onNextMoveComplete();
		        });
			
				this.opts.$scroll.addClass( 'with-transition' );
			}
			
			this.vars.intCurrentStart = ( this.vars.intCurrentStart + 1 ) == this.vars.arrItems.length ? 0 : ( this.vars.intCurrentStart + 1 );
			this.vars.intCurrentEnd = ( this.vars.intCurrentEnd + 1 ) == this.vars.arrItems.length ? 0 : ( this.vars.intCurrentEnd + 1 );
			
			$.preload( [ this.vars.arrItems[ this.vars.intCurrentEnd ].image ], {
				loaded_all: function( loaded, total ) {
					_this.addItemToEnd( _this.vars.intCurrentEnd );
			        var intToMove = $( _.first( _this.opts.$scroll.find( '.item' ) ) ).outerWidth( true );
			        _this.shiftScroller( -intToMove, true, _this.onNextMoveComplete );


					if( !$.support.transition ){
						_this.onNextMoveComplete();
					}
				}
			});
			
			
	        
		}
		
		this.addNavListeners = function( ){
			var _this = this;
			
			this.opts.$leftnav.on( 'click', function( e ){
				e.preventDefault();
				
				if( !_this.vars.animating ){
					_this.vars.animating = true;
					_this.onPrevClick();
				}
			});
			
			this.opts.$rightnav.on( 'click', function( e ){
				e.preventDefault();
				
				if( !_this.vars.animating ){
					_this.vars.animating = true;
					_this.onNextClick();
				}
			});
		}
		
		this.addWindowListener = function( ){
			var _this = this;
			
			var lazyLayout = _.debounce( function( ){
				_this.resizeScroller( );
				_this.centerScroller( );
			}, 300);
			
			$(window).resize( lazyLayout );
		}
		
		this.centerScroller = function( ){
			var intItemWidth = 940;
			
			if( this.opts.$window.width( ) < 940 )
				intItemWidth = this.opts.$window.width( );
			
	        var intExcessSpace = ( this.opts.$window.width( ) - intItemWidth ) / 2;
			
	        this.opts.$scroll.css( 'left', -( intItemWidth - intExcessSpace ) );
		}
		
		this.resizeScroller = function( ){
			if( this.opts.$window.width( ) < this.vars.intCurrentImageWidth ){
				var width = this.opts.$window.width( ),
					height = ( width / 100 ) * 49.47;
	            this.opts.$scroll.find( '.item' ).css( 'width', width );
	            this.opts.$parent.css( 'height', height );
	            this.vars.intCurrentImageWidth = width;
	        }else if( this.opts.$parent.css( 'height' ) != "508px" ){
	            this.opts.$scroll.find( '.item' ).css( 'width', 'auto' );
	            this.opts.$parent.css( 'height', "508px" );

	            this.vars.intCurrentImageWidth = 940;
	        }
		}
		
		this.removeAllItems = function( ){
			this.opts.$scroll.find( '.item' ).remove();
		}
		
		this.removeExcessEnd = function( ){
			this.opts.$scroll.find( '.item' ).each( function( i ){
	            if( i > 2 ){
	                $( this ).remove();
	            }
	        });
		}
		
		this.addItemToStart = function( arrId ){
			//console.log( arrId );
			var strItem = _.template( templateItem, { jsid: this.vars.arrItems[arrId].id, link: this.vars.arrItems[arrId].link, id: arrId, imagewidth: this.vars.intCurrentImageWidth, image: this.vars.arrItems[arrId].image, caption: this.vars.arrItems[arrId].description });
			this.opts.$scroll.prepend( strItem );
		}
		
		this.addItemToEnd = function( arrId ){
			var strItem = _.template( templateItem,  { jsid: this.vars.arrItems[arrId].id, link: this.vars.arrItems[arrId].link, id: arrId, imagewidth: this.vars.intCurrentImageWidth, image: this.vars.arrItems[arrId].image, caption: this.vars.arrItems[arrId].description } );
	        this.opts.$scroll.append( strItem );
			
		}
		
		this.removeFromStart = function(){
			$( _.first( this.opts.$scroll.find( '.item' ) ) ).remove();
		}
		
		this.removeFromEnd = function(){
			$( _.last( this.opts.$scroll.find( '.item' ) ) ).remove();
		}
		
		this.setupScroller = function( ){
	        
	        this.removeAllItems( );
			this.addItemToStart( 1 ); // put the last item to the start
			this.addItemToStart( 0 ); // put the last item to the start
			this.addItemToStart( ( this.vars.arrItems.length - 1 ) ); // put the last item to the start
	        
			this.resizeScroller( );
	        this.centerScroller( );
	
			this.opts.$parent.addClass( 'show' );
	    }
		
		this.parseItems = function( ){
			var _this = this,
				booMobile = false;
				
			if ( matchMedia('only screen and (max-width: 480px)').matches )
				booMobile = true;
			
			this.opts.$parent.find( '.item' ).each( function( e ){
	            var objItem = {},
	            $this = $( this );
				
				
				if( !booMobile )
	            	objItem.image = $this.find('[data-picture]').data( 'src' );
				else
					objItem.image = $this.find('[data-picture]').data( 'mobilesrc' );
				
				
				objItem.description = $this.find( '.carousel-caption' ).html();
				objItem.link = $this.find( 'a' ).attr( 'href');
				objItem.id = $this.attr( 'id');

	            _this.vars.arrItems.push( objItem );
	        });

	        this.vars.intCurrentStart = this.vars.arrItems.length - 1;
	        this.vars.intCurrentEnd = 1;
	
			var arrPreload = [
				this.vars.arrItems[0].image,
				this.vars.arrItems[1].image,
				this.vars.arrItems[this.vars.arrItems.length - 1].image
			]
	
			$.preload( arrPreload, {
				loaded_all: function( loaded, total ) {
					
					_this.setupScroller( );
					_this.resetActive( );
					_this.addWindowListener( );
					_this.addNavListeners( );

					_this.checkBranding();
					
					
				}
			});
	
		}
		
		this.changeBranding = function( state ){
			var newImage = '/assets/img/branding.png',
				$branding = $( '.carousel-branding' ),
				$currentImage = $branding.find( 'img' );
			
			if( state == 'dark' )
				newImage = '/assets/img/branding-dark.png';
			
			if( $.support.transition ){
				$currentImage.one( $.support.transition.end, function () {
					$currentImage.remove();
			    });
			}else
				$currentImage.remove();
				
			$branding.prepend( '<img src="'+newImage+'" />');
			$currentImage.addClass( 'hide' );
			
			this.vars.activeColourState = state;
		}
		
		this.checkBranding = function( ){
			var activeColourState = this.opts.$scroll.find( '.active' ).attr( 'id' ).replace( 'image_', '' );
		
			if( activeColourState != this.vars.activeColourState ){
		        this.changeBranding( activeColourState );
			}
		}
		
		this.init = function( ){
			this.vars.intCurrentImageWidth = 940;
			//this.resizeScroller();
			this.parseItems( );
		}
		
		this.init();
	}
	
	return Carousel;
});