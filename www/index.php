<?php
/**
 * Custom functions
 */


function fetchInstagramData( ){
    $transient = "instagram";
    $expiration = 1800; // 30 mins

    $token = '194955960.ab103e5.0d95188b2a6845e383cf93207553cdef';
    $insta_id = '194955960';

    //Your token is: 194955960.ab103e5.0d95188b2a6845e383cf93207553cdef 
	// Your user ID is: 194955960
   	$url = "https://api.instagram.com/v1/users/".$insta_id."/media/recent/?access_token=".$token."&count=10";

   	// https://instagram.com/oauth/authorize/?client_id=CLIENT-ID&redirect_uri=REDIRECT-URI&response_type=token

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    $result = curl_exec($ch);
    curl_close($ch);
    
    return json_decode($result);
}

$instagram_data = fetchInstagramData();

?>

<!DOCTYPE>
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10 ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">

	<link rel="stylesheet" href="assets/css/main.css?bust=@@bust">

	<!-- Custom Modernizr is generated via grunt on build -->
	<script src="assets/js/lib/modernizr-2.6.2.js"></script>

	<!-- Add media query support for IE8 -->
	<!--[if (gte IE 6)&(lte IE 8)]>
		<script type="text/javascript" src="js/lib/respond.js"></script>
	<![endif]-->
</head>

<body>

	<div role="main">

		<header class="hd-main">
			<h1 class="hd-logo">Home Bakery</h1>
			<h2 class="hd-telephone"><strong>Telephone</strong> 05 5263 3004</h2>
			
			<nav class="nav-share">
				<ul>
					<!--<li class="fb"><a href="https://www.facebook.com/huslerandrose" target="_blank">Facebook</a></li>-->
					<li class="twitter"><a href="https://twitter.com/The_home_bakery" target="_blank">Twitter</a></li>
					<li class="insta"><a href="http://instagram.com/home_bakery" target="_blank">Instagram</a></li>
				</ul>
			</nav>
		</header>



				<div id="carousel" class="owl-carousel">
				<?
					foreach ($instagram_data->data as $post): ?>
						<div class="item">
							<img class="lazyOwl" data-src="<?=$post->images->standard_resolution->url;?>" alt="Lazy Owl Image" />
						</div>

			<?
					endforeach; ?>
            	</div> 

		<footer class="ft-main">
			<small>Simple indulgence delivered across the middle east since 2011</small>
		</footer>

	</div>

	<script src="assets/js/lib/require-jquery.js"></script>
	<script>
		require(['./assets/js/common'], function (common) {
        	require(['home']);
    	});
	</script>

	<script src="http://127.0.0.1:35729/livereload.js"></script>

	<!--
	<script>
	var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
		g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)}(document,'script'));
	</script>
	-->

</body>
</html>