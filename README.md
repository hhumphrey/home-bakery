# A minimal boilerplate for a flat html build.

## Setup
To setup a new project with the node modules required to build, navigate to your project folder in terminal and type the command 'npm install'.

* The grunt command line interface must be installed globally. See http://gruntjs.com/getting-started. *

# Grunt commands

## grunt dev

The command 'grunt dev', run from inside the root folder, initiates watching for your project.

The watch task includes .less compilation, livereload, and CSS autoprefixing.

## grunt build

The command 'grunt build', compiles, uglifies, concatenates dependencies, optimises image assets and builds to a deployment folder, 'www-deploy'.

A custom Modernizr file is built, based on the usage in your js/css, and referenced in the head of the build file. Css/js Cache busting uses the package number (see package.json).

##grunt bump

The bump command increments the package number (this is used to cache busting css/js assets on build), tags the repo as vX.X.X, and commits/pushes to the origin.

##grunt deploy

This shows how to configure grunt to deploy a folder over FTP. Authentication details are in .ftppass.

